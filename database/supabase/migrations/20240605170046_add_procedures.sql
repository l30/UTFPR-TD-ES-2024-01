CREATE OR REPLACE FUNCTION get_saldo()
RETURNS MONEY
LANGUAGE plpgsql
AS $$
DECLARE
    saldo MONEY;
BEGIN
    SELECT ultimo_saldo INTO saldo FROM transacao
    ORDER BY transacao_id DESC
    LIMIT 1;
    IF NOT FOUND THEN
        RETURN CAST(0 AS MONEY);
    END IF;
    RETURN saldo;
END;
$$;

CREATE OR REPLACE PROCEDURE adicionar_valor(
    tipo CHAR(20),
    valor INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO transacao (tipo, valor, operacao, ultimo_saldo)
    VALUES (tipo, CAST(valor AS MONEY), 'E', get_saldo() + CAST(valor AS MONEY));
END;
$$;

CREATE OR REPLACE PROCEDURE remover_valor(
    tipo CHAR(20),
    valor MONEY
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO transacao (tipo, valor, operacao, ultimo_saldo)
    VALUES (tipo, valor, 'S', get_saldo() - valor);
END;
$$;

CREATE OR REPLACE FUNCTION get_ultima_transacao_id()
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
    id INT;
BEGIN
    SELECT transacao_id INTO id FROM transacao
    ORDER BY transacao_id DESC
    LIMIT 1;
    RETURN id;
END;
$$;

CREATE OR REPLACE PROCEDURE registrar_fornecedor( 
    nome VARCHAR(255), 
    cnpj VARCHAR(10)
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO fornecedor (nome, cnpj) VALUES (nome, cnpj);
END;
$$;


CREATE OR REPLACE PROCEDURE registrar_produto(
    nome    VARCHAR(255),
    barcode CHAR(13)
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO produto(nome, codigo_de_barra) VALUES(nome, barcode);
END;
$$;


CREATE OR REPLACE PROCEDURE atualizar_preco_produto(
    produto_id INT,
    novo_preco MONEY
)
LANGUAGE plpgsql
AS $$
BEGIN
    UPDATE produto p
    SET valor_venda = novo_preco
    WHERE p.produto_id = $1;
END;
$$;


CREATE OR REPLACE PROCEDURE atualizar_quant_min(
    produto_id        INT,
    quant_min_estoque INT,
    quant_min_gondola INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    UPDATE produto p
    SET quantidade_min_estoque = $2,
        quantidade_min_gondola = $3
    WHERE p.produto_id = $1;
END;
$$;

CREATE OR REPLACE PROCEDURE atualiza_quant_gondola(
    produto_id        INT,
    quant_gondola INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    UPDATE produto p
    SET quantidade_gondola = $2
    WHERE p.produto_id = $1;
END;
$$;

-- ==========   PEDIDOS   ==========

CREATE OR REPLACE PROCEDURE abrir_pedido(
    fornecedor_id INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO pedido(fornecedor_id)
    VALUES (fornecedor_id);
END;
$$;


CREATE OR REPLACE FUNCTION get_ultimo_pedido_id()
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
    id INT;
BEGIN
    SELECT pedido_id INTO id FROM pedido
    ORDER BY pedido_id DESC
    LIMIT 1;
    RETURN id;
END;
$$;

CREATE OR REPLACE PROCEDURE adicionar_pedido_produto(
    pedido_id  INT,
    produto_id INT,
    quantidade INT,
    valor      MONEY
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO pedido_produto(produto_id, pedido_id, quantidade, valor)
    VALUES (produto_id, pedido_id, quantidade, valor);
END;
$$;


CREATE OR REPLACE FUNCTION get_valor_total_pedido(pedido_id INT)
RETURNS MONEY
LANGUAGE plpgsql
AS $$
DECLARE
    valor_total MONEY;
BEGIN
    SELECT SUM(pp.valor) INTO valor_total
    FROM pedido_produto pp
    WHERE pp.pedido_id = $1;
    RETURN valor_total;
END;
$$;


CREATE OR REPLACE PROCEDURE fechar_pedido(
    pedido_id     INT,
    data_prevista DATE
)
LANGUAGE plpgsql
AS $$
BEGIN
    CALL remover_valor('Compra', get_valor_total_pedido($1));
    UPDATE pedido p
    SET status = 'A caminho',
        data_prevista = $2,
        transacao_id = get_ultima_transacao_id()
    WHERE p.pedido_id = $1;
END;
$$;



CREATE OR REPLACE PROCEDURE pedido_chegou(
    pedido_id INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    UPDATE pedido p
    SET status = 'Chegou',
        data_chegada = CURRENT_DATE
    WHERE p.pedido_id = $1;
END;
$$;


CREATE OR REPLACE FUNCTION get_valores_compra(produto_id INT)
RETURNS TABLE (valor_un MONEY)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
      SELECT pp.valor/pp.quantidade
      FROM pedido_produto pp
      WHERE pp.produto_id = $1;
END;
$$;


CREATE OR REPLACE FUNCTION get_nome_from_id(produto_id INT)
RETURNS VARCHAR(255)
LANGUAGE plpgsql
AS $$
DECLARE 
      nome VARCHAR(255);
BEGIN
      SELECT p.nome INTO nome
      FROM produto p
      WHERE p.produto_id = $1;
      RETURN nome;
END;
$$;



-- ==========   QUANTIDADES E MOVIMENTAÇÕES   ==========

CREATE OR REPLACE PROCEDURE adicionar_produto_estoque(
    produto_id INT,
    quantidade INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    UPDATE produto p
    SET quantidade_estoque = quantidade_estoque + $2
    WHERE p.produto_id = $1;
END;
$$;

CREATE OR REPLACE PROCEDURE remove_produto_gondola(
    produto_id INT,
    quantidade INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    UPDATE produto p
    SET quantidade_estoque = quantidade_estoque - $2
    WHERE p.produto_id = $1;
END;
$$;

CREATE OR REPLACE FUNCTION get_estoq_from_barcode(barcode CHAR(13))
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
    quantidade_estoque INT;
BEGIN
    SELECT p.quantidade_estoque INTO quantidade_estoque
    FROM produto p
    WHERE p.codigo_de_barra = $1;
    RETURN quantidade_estoque;
END;
$$;

CREATE OR REPLACE PROCEDURE mover_produto_estoque_gondola(
    produto_id INT,
    quantidade INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    UPDATE produto p
    SET quantidade_estoque = quantidade_estoque - $2,
        quantidade_gondola = quantidade_gondola + $2
    WHERE p.produto_id = $1;
END;
$$;

