CREATE TABLE IF NOT EXISTS fornecedor(
    fornecedor_id      INT GENERATED ALWAYS AS IDENTITY,
    nome    VARCHAR(255) NOT NULL,
    cnpj    VARCHAR(14)  NOT NULL UNIQUE,
    PRIMARY KEY (fornecedor_id)
);

CREATE TABLE IF NOT EXISTS produto(
    produto_id      INT GENERATED ALWAYS AS IDENTITY,
    nome            VARCHAR(255) NOT NULL UNIQUE,
    codigo_de_barra CHAR(13) NOT NULL CHECK(LENGTH(codigo_de_barra)=13),
    valor_venda     MONEY,
    quantidade_gondola         INT DEFAULT 0,
    quantidade_estoque         INT DEFAULT 0,
    quantidade_min_gondola     INT,
    quantidade_min_estoque     INT,
    PRIMARY KEY (produto_id)
);

CREATE TABLE IF NOT EXISTS transacao(
    transacao_id    INT GENERATED ALWAYS AS IDENTITY,
    tipo            CHAR(20),
    valor           MONEY,
    data            TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    operacao        CHAR(1),
    ultimo_saldo    MONEY,
    PRIMARY KEY(transacao_id)
);

CREATE TABLE IF NOT EXISTS pedido(
    pedido_id       INT GENERATED ALWAYS AS IDENTITY,
    fornecedor_id   INT NOT NULL,
    transacao_id    INT,
    data_prevista   DATE,
    data_chegada    DATE,
    status          VARCHAR(20) DEFAULT 'Aberto',
    PRIMARY KEY(pedido_id),
    CONSTRAINT fk_fornecedor
        FOREIGN KEY(fornecedor_id)
            REFERENCES fornecedor(fornecedor_id),
    CONSTRAINT fk_transacao
        FOREIGN KEY(transacao_id)
            REFERENCES transacao(transacao_id)
);

CREATE TABLE IF NOT EXISTS pedido_produto(
    pedido_produto_id   INT GENERATED ALWAYS AS IDENTITY,
    pedido_id   INT NOT NULL,
    produto_id  INT NOT NULL,
    quantidade  INT NOT NULL CHECK(quantidade > 0),
    valor       MONEY NOT NULL,
    PRIMARY KEY (pedido_produto_id),
    CONSTRAINT fk_pedido
        FOREIGN KEY(pedido_id)
            REFERENCES pedido(pedido_id),
    CONSTRAINT fk_produto
        FOREIGN KEY(produto_id)
            REFERENCES produto(produto_id)
);
