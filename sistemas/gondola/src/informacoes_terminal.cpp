#include <iostream>
#include "../include/informacoes_terminal.hpp"

using namespace std;

void imprime_comandos(int i){
    switch(i){
        case 0:
            cout << "\n*******************************************************\n";
            cout << "*  Digite: 1 - Para iniciar uma venda.                *\n";
            cout << "*          2 - Para finalizar a execucao do sistema.  *\n";
            cout << "*******************************************************\n";
            cout << "Opcao: ";
            break;
        case 1:
            cout << " »      AVISO: entrada invalida, escolha entre       « ";
            cout << "\n »                 os numeros 1 e 2.                  « \n\n";
            break;
        case 2:
            cout << "\n------------------Iniciando uma venda------------------\n";
            cout << "|                                                     |\n";
            cout << "|  Digite: -1 Para finalizar a venda.                 |\n";
            cout << "|          -2 Para remover produtos da venda.         |\n";
            cout << "|          -3 Para cancelar a venda.                  |\n";
            cout << ":                                                     :\n";
            cout << ":                                                     :\n";
            break;
        case 3:
            cout << "\n   »  ERRO: Entrada invalida, digite um codigo de  «   \n";
            cout << "            »  produto maior que 0.  «             \n " << endl;
            break;
        case 4:
            cout << ":                                                     :\n";
            cout << ":~~~~~~~~~~~~~~~~~ Removendo produto ~~~~~~~~~~~~~~~~~:\n";
            break;
        case 5:
            cout << ":                                                     :\n";
            cout << ":~~~~~~~~~~~~~~~~ Continuando a compra ~~~~~~~~~~~~~~~:\n";
            cout << ":                                                     :\n";
            break;
        case 6:
            cout << "\n|                                                     |\n";
            cout << "|~~~~~~~~~~~~~~ Sucesso no Cancelamento ~~~~~~~~~~~~~~|\n\n";
            break;
        case 7:
            cout << ":                                                     :\n";
            cout << ":~~~~~~~~~~~~~~~~~~~~~ Pagamento ~~~~~~~~~~~~~~~~~~~~~:\n";
            cout << ":                                                     :\n";
            break;
	case 8:
	    cout << "\n\n     »     ERRO: Produto nao existe na venda       «   \n";
	    break;
        case 9:
            cout << "   »       ERRO: Existe menos produto na venda      «  \n";
            cout << "   »            do que deseja-se retirar            «  \n";
            break;
        case 10:
            cout << "             →    Produto foi removido.    ←           \n";
            break;
        case 11:
	    cout << "\n Forma de pagamento: 1. Dinheiro" << endl;
	    cout << "                     2. Cartao de Debito " << endl;
	    cout << "                     3. Cartao de Credito" << endl;
	    cout << " Escolha: ";
            break;
	case 12:
	    cout << "\n:                                                     :\n";
	    cout << "|                                                     |\n";
	    cout << "-------------------Compra finalizada-------------------\n\n";
	    break;
    }
}
