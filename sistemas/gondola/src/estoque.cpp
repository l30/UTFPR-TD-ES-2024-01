#include <iostream>
#include <cstring>
#include "../include/estoque.hpp"

using namespace std;


void Estoque::consulta_estoque(trie *arvore){
	int codigo, *codigo_transformado;
	bloco *buscado;

	while(1){
	        cout << "\nDigite um codigo de barras ou -1 para sair da consulta: ";
        	cin >> codigo;

        	if(codigo <= 0) break;

        	codigo_transformado = transforma_codigo(codigo);
        	buscado = busca_codigo(arvore, codigo_transformado);

        	if(buscado == NULL) {
         	   	cout << "Codigo nao encontrado" << endl;
        	}
        	else {
           		cout << "\n   Produto: " << buscado->descricao << endl;
            		cout << "   Quantidade em estoque: " << buscado->quantidade;
            	
			if(strcmp(buscado->tipo, "KG") == 0)
				cout << " kilogramas" << endl;
			else
				cout << " unidades" << endl;
		
			cout << "\n" ;
		}

	}
}
