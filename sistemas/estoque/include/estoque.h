#ifndef ESTOQUE_C
#define ESTOQUE_C

#include <libpq-fe.h>

#ifdef __cplusplus
extern "C" {
#endif

// remove `count' items do estoque com o codigo de barras `barcode'
void estoque_remove_item_count(PGconn* conn, const char* barcode, size_t count);

#ifdef __cplusplus
}
#endif

#endif
