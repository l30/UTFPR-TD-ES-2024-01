#include "../include/estoque.h"

#include <libpq-fe.h>

#include <string>
#include <iostream>

using namespace std;

void estoque_remove_item_count(PGconn* conn, const char* _barcode, size_t count) {
	if (!_barcode) return;

	string barcode { _barcode };
	string query =
		"SELECT quant_estoque"
		" FROM quant_produtos"
		" WHERE codigo_de_barra = '" + barcode + "';";

	PGresult* res = PQexec(conn, query.c_str());
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		cout << "Produto com código de barras " << barcode << " não encontrado" << endl;
		return;
	}

	int res_len = PQntuples(res);
	if (res_len != 1) {
		cout << "Mais de um produto com o código de barra " << barcode << " encontrado" << endl;
		return;
	}

	int col_len = PQnfields(res);
	if (col_len != 1) {
		cout << "Mais de uma coluna" << endl;
		return;
	}

	int quant_estoque = 0;
	for (size_t i = 0; i < col_len; i++) {
		string col_name { PQfname(res, i) };
		if (col_name == "quant_estoque") {
			quant_estoque = atoi(PQgetvalue(res, 0, i));
			break;
		}
	}

	PQclear(res);

	if (((long)quant_estoque - (long)count) < 0) {
		cout << "Estoque não tem items suficientes" << endl;
		return;
	}

	query =
		"UPDATE quant_produtos"
		" SET quant_estoque = quant_estoque - " + to_string(count) +
		" WHERE codigo_de_barra = '" + barcode + "'";

	cout << "\"" << query << "\"" << endl;

	res = PQexec(conn, query.c_str());
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		cout << "Não possível atualizar o banco de dados" << endl;
		return;
	}

	PQclear(res);
}
