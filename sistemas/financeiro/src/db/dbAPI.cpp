#include <iostream>
#include <string>
#include <time.h> 
#include <vector>
#include "../../include/db/dbAPI.hpp"

std::string random_barcode(){
    srand (time(NULL));
    std::string code = "789";
    while (code.size()<12){
        int randint = rand() %10;
        code = code + std::to_string(randint);
    }
    while (code.size()>13){
        code.pop_back();
    }
    int sum_even = 0;
    int sum_odd = 0;
    for(int i = 1; i <= 12; i = i+2){
        sum_even = sum_even + (code[i] - '0'); 
        sum_odd = sum_odd + (code[i-1] - '0');
    }
    int sum = sum_even*3 + sum_odd;
    int remainder = sum % 10;
    int check = 10 - remainder;
    if(check == 10) check = 0;
    code += std::to_string(check);
    return code;
}

mercado_dbAPI::mercado_dbAPI(){
    conn = PQconnectdb("postgres://employee.lcbbxrcxfadaaqdvoyzx:senha@aws-0-sa-east-1.pooler.supabase.com:6543/postgres");
}

mercado_dbAPI::mercado_dbAPI(std::string conn_type){
    if(conn_type.compare("local") == 0)
        conn = PQconnectdb("postgresql://employee:senha@localhost:54322/postgres");
    else
        conn = PQconnectdb("postgres://employee.lcbbxrcxfadaaqdvoyzx:senha@aws-0-sa-east-1.pooler.supabase.com:6543/postgres");
}
mercado_dbAPI::~mercado_dbAPI(){
    PQfinish(conn);
}

int mercado_dbAPI::execCommand(std::string command){
    PGresult *res = PQexec(conn, command.c_str());
    if(PQresultStatus(res) != PGRES_COMMAND_OK){
        PQclear(res);
        return 0;
    }
    PQclear(res);
    return 1;
}

int mercado_dbAPI::testConn(){
    if (PQstatus(conn) == CONNECTION_BAD) // Testando conexao
        return 0;
    return 1;
}

int mercado_dbAPI::adiciona_saldo(std::string tipo, int valor){
    std::string command = "CALL adicionar_valor('"+ tipo +"', " + std::to_string(valor) + ");";
    return execCommand(command);
}

int mercado_dbAPI::registra_fornecedor(std::string nome, std::string cnpj){
    std::string command = "CALL registrar_fornecedor('" + nome + "', '" + cnpj + "');";
    return execCommand(command);
}

int mercado_dbAPI::registra_produto(std::string nome){
    std::string command = "CALL registrar_produto('" + nome + "', '" + random_barcode() + "');";
    return execCommand(command);
}

int mercado_dbAPI::atualiza_preco_produto(int produto_id, float novo_preco){
    std::string command = "CALL atualizar_preco_produto(" + std::to_string(produto_id) + ", CAST(" + std::to_string(novo_preco) + " AS MONEY));";
    return execCommand(command);
}

int mercado_dbAPI::atualiza_quant_min_produto(int produto_id, int quant_min_estoque, int quant_min_gondola){
    std::string command = "CALL atualizar_quant_min("+ std::to_string(produto_id) + "," + std::to_string(quant_min_estoque) + "," + std::to_string(quant_min_gondola) + ");";
    return execCommand(command);
}

int mercado_dbAPI::atualiza_quant_gondola(int produto_id, int quantidade){
    std::string command = "CALL atualiza_quant_gondola("+ std::to_string(produto_id) + "," + std::to_string(quantidade) + ");";
    return execCommand(command);
}

int mercado_dbAPI::abre_pedido(int fornecedor_id){
    std::string command = "CALL abrir_pedido(" + std::to_string(fornecedor_id) + ");";
    execCommand(command);
    return get_ultimo_pedido_id();
}

int mercado_dbAPI::get_ultimo_pedido_id(){
    int pedido_id = 0;
    PGresult *res = PQexec(conn, "SELECT get_ultimo_pedido_id();");

    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        return -1;
    }
    
    pedido_id = std::stoi(PQgetvalue(res,0,0));
    PQclear(res);
    return pedido_id;
}

int mercado_dbAPI::fecha_pedido(int pedido_id, std::string data_prevista){
    std::string command = "CALL fechar_pedido("+ std::to_string(pedido_id) +", CAST('" + data_prevista + "' AS DATE));";
    return execCommand(command);
}

int mercado_dbAPI::adiciona_pedido_produto(int pedido_id, int produto_id, int quantidade, float valor){
    std::string command = "CALL adicionar_pedido_produto("+ std::to_string(pedido_id) +","+ std::to_string(produto_id) + "," + std::to_string(quantidade) + ", CAST('" + std::to_string(valor) + "' AS MONEY));";
    return execCommand(command);
}

std::vector<std::string> mercado_dbAPI::get_valores_compra(int produto_id){
    std::string command = "SELECT get_valores_compra(" + std::to_string(produto_id) + ");";
    PGresult *res = PQexec(conn, command.c_str());

    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        
    }

    int rec_count = PQntuples(res);
    

    std::vector<std::string> valores;
    for (int row=0; row<rec_count; row++){
        valores.push_back(PQgetvalue(res,row,0));
    }
    PQclear(res);
    return valores;
}


std::string mercado_dbAPI::get_nome_produto(int produto_id){
    std::string command = "SELECT get_nome_from_id("+ std::to_string(produto_id) +")";
    PGresult *res = PQexec(conn, command.c_str());

    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        
        return "Produto invalido";
    }
    std::string produto_nome = PQgetvalue(res,0,0);
    PQclear(res);
    return produto_nome;
}

std::string mercado_dbAPI::get_saldo(){
    std::string command = "SELECT get_saldo()";
    PGresult *res = PQexec(conn, command.c_str());

    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
      return "ERRO na chamada get_saldo()";    
    }
    std::string saldo = PQgetvalue(res,0,0);
    PQclear(res);
    return saldo;
}


int mercado_dbAPI::add_produto_estoque(int produto_id, int quantidade){
    std::string command = "CALL adicionar_produto_estoque("+ std::to_string(produto_id) + "," + std::to_string(quantidade) +");";
    return execCommand(command);
}

int mercado_dbAPI::move_produto_estoque_gondola(int produto_id, int quantidade){
    std::string command = "CALL mover_produto_estoque_gondola("+ std::to_string(produto_id) + "," + std::to_string(quantidade) +");";
    return execCommand(command);
}

std::vector<std::vector<std::string>> mercado_dbAPI::tabela_from_query(std::string query){
    
    std::vector<std::vector<std::string>> dados;
    PGresult *res; 
    
    res = PQexec(conn, query.c_str());  // Mandando query para db
    
    if (PQresultStatus(res) != PGRES_TUPLES_OK) 
        return dados;
    
    int rec_count = PQntuples(res);
    int max_col = PQnfields(res);

    for (int row=0; row<rec_count; row++) {
        std::vector<std::string> linha;
        for (int col=0; col<max_col; col++)
            linha.push_back(PQgetvalue(res, row, col));
        dados.push_back(linha);
    }
    
    return dados;
}

std::vector<std::vector<std::string>> mercado_dbAPI::get_tabela(std::string tabela){
    std::vector<std::vector<std::string>> dados;
    std::string query = "SELECT * FROM " + tabela + " ORDER BY 1 ASC;"; 
    dados = tabela_from_query(query);
    return dados;
}

std::vector<std::vector<std::string>> mercado_dbAPI::get_produtos_caminho(){
    std::vector<std::vector<std::string>> dados;
    std::string query = "SELECT * \
                          FROM  pedido \
                          WHERE status = 'A caminho';"; 
    dados = tabela_from_query(query);
    return dados;
}

std::vector<std::vector<std::string>> mercado_dbAPI::get_produtos_pedido(int pedido_id){
    
    std::vector<std::vector<std::string>> dados;
    std::string query = "SELECT p.nome, pp.quantidade, pp.valor \
                         FROM  pedido_produto pp\
                         INNER JOIN produto p ON pp.produto_id = p.produto_id\
                         WHERE pp.pedido_id = "+ std::to_string(pedido_id) +";"; 
    
    dados = tabela_from_query(query);
    return dados;
}

std::vector<std::vector<std::string>> mercado_dbAPI::get_produtos_com_preco(){
    
    std::vector<std::vector<std::string>> dados;
    std::string query = "SELECT * \
                         FROM  produto p\
                         WHERE p.valor_venda IS NOT NULL;"; 
    
    dados = tabela_from_query(query);
    return dados;
}


std::optional<std::string> mercado_dbAPI::callFunction(std::string function){
    std::string command = "SELECT " + function;
    PGresult *res = PQexec(conn, command.c_str());
    if (PQresultStatus(res) != PGRES_TUPLES_OK){
        PQclear(res);
        return std::nullopt;
    }
    std::string result = PQgetvalue(res,0,0);
    PQclear(res);
    return result;
}

std::optional<int> mercado_dbAPI::get_quant_estoque_from_barcode(std::string barcode){
    std::string function = "get_estoq_from_barcode('" + barcode + "');";
    auto result = callFunction(function);
    if(result.has_value())
        return stoi(result.value());

    return std::nullopt;
}
