#include "../../include/financeiro/cadastra_produto.hpp"
#include <string>

Cadastra_produto::Cadastra_produto()
:
    confirm_button("Confirmar"),
    quit_button("Sair"),
    v_box(Gtk::Orientation::VERTICAL),
    label_name("Nome do forncedor: ", Gtk::Align::START),
    label_aviso("", Gtk::Align::START)
{
    
    set_default_size(200, 100);
    set_title("Cadastra forncedor");

    set_child(v_box);
    
    

    label_name.set_expand();
    v_box.append(label_name);
    v_box.append(entry_name);
    entry_name.set_expand();

    v_box.set_spacing(5);


    v_box.append(label_aviso);
    v_box.append(h_box_button);

    quit_button.signal_clicked().connect( sigc::mem_fun(*this,
              &Cadastra_produto::on_quit) );
    h_box_button.append(quit_button);
    quit_button.set_expand();

    confirm_button.signal_clicked().connect( sigc::mem_fun(*this,
              &Cadastra_produto::on_confirm) );
    h_box_button.set_spacing(5);
    h_box_button.append(confirm_button);
    confirm_button.set_expand();

    v_box.set_spacing(10);

}

void Cadastra_produto::on_confirm()
{   

    db->registra_produto(entry_name.get_text());
    label_aviso.set_text("Cadastrado com sucesso");
    
}

void Cadastra_produto::on_quit()
{
    set_visible(false);
}


void Cadastra_produto::set_db(mercado_dbAPI* db_financeiro)
{
    db = db_financeiro;
    label_aviso.set_text("");
    entry_name.set_text("");
    
}