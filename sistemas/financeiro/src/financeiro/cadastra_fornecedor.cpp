#include "../../include/financeiro/cadastra_fornecedor.hpp"
#include <string>

Cadastra_fornecedor::Cadastra_fornecedor()
:
    confirm_button("Confirmar"),
    quit_button("Sair"),
    v_box(Gtk::Orientation::VERTICAL),
    v_box_cnpj(Gtk::Orientation::VERTICAL),
    v_box_name(Gtk::Orientation::VERTICAL),
    label_cnpj("CNPJ: ", Gtk::Align::START),
    label_name("Nome do forncedor: ", Gtk::Align::START),
    label_aviso("", Gtk::Align::START)
{
    
    set_default_size(200, 100);
    set_title("Cadastra forncedor");

    set_child(v_box);
    v_box.append(v_box_name);
    v_box.append(v_box_cnpj);
    v_box.append(label_aviso);
    v_box.append(h_box_button);

    label_name.set_expand();
    v_box_name.append(label_name);
    v_box_name.append(entry_name);
    entry_name.set_expand();

    v_box_name.set_spacing(5);

    label_cnpj.set_expand();
    v_box_cnpj.append(label_cnpj);
    v_box_cnpj.append(entry_cnpj);
    entry_cnpj.set_expand();

    v_box_cnpj.set_spacing(5);


    quit_button.signal_clicked().connect( sigc::mem_fun(*this,
              &Cadastra_fornecedor::on_quit) );
    h_box_button.append(quit_button);
    quit_button.set_expand();

    confirm_button.signal_clicked().connect( sigc::mem_fun(*this,
              &Cadastra_fornecedor::on_confirm) );
    h_box_button.set_spacing(5);
    h_box_button.append(confirm_button);
    confirm_button.set_expand();

    v_box.set_spacing(10);

}

void Cadastra_fornecedor::on_confirm()
{   
   
    db->registra_fornecedor(entry_name.get_text(), entry_cnpj.get_text());
    label_aviso.set_text("Cadastrado com sucesso");
    
}

void Cadastra_fornecedor::on_quit()
{
    set_visible(false);
}


void Cadastra_fornecedor::set_db(mercado_dbAPI* db_financeiro)
{
    db = db_financeiro;
    label_aviso.set_text("");
    entry_name.set_text("");
    entry_cnpj.set_text("");
    
}