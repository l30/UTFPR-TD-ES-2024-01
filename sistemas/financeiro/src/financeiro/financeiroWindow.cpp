#include "../../include/financeiro/financeiroWindow.hpp"
#include <iostream>
#include "financeiroWindow.hpp"


FinanceiroW::FinanceiroW()
:
    db(mercado_dbAPI()),
    f_patrimonio("Patrimonio"),
    cadastra_fornecedor_b("Cadastrar fornecedor"),
    cadastra_produto_b("Cadastrar produto"),
    define_info_venda_produto_b("Adicionar valor de venda"),
    fazer_pedido_b("Fazer pedido"),
    consulta_preco_compra_b("Consultar valor de compra"),
    mostra_todos_produtos_b("Mostrar todos os produtos"),
    mostra_fornecedores_b("Mostrar os fornecedores"),
    mostra_produtos_caminho_b("Mostrar produtos a caminho"),
    mostra_produtos_pedido_b("Mostrar pedidos de produtos"),
    quit_button("Sair"),
    v_box(Gtk::Orientation::VERTICAL)
{
    
    set_default_size(200, 100);
    set_title("Financeiro");

    set_child(v_box);

    v_box.append(f_patrimonio);
    f_patrimonio.set_child(h_patrimonio);

    h_patrimonio.append(l_patrimonio);

    b_patrimonio.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::reload_patrimonio) );
    b_patrimonio.set_icon_name("object-rotate-left");
    h_patrimonio.append(b_patrimonio);
    l_patrimonio.set_expand();

    v_box.append(cadastra_produto_b);
    cadastra_produto_b.set_expand();
    cadastra_produto_b.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::cadastra_produto) );
    

    v_box.append(define_info_venda_produto_b);
    define_info_venda_produto_b.set_expand();
    define_info_venda_produto_b.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::define_info_venda_produto) );
    
    v_box.append(cadastra_fornecedor_b);
    cadastra_fornecedor_b.set_expand();
    cadastra_fornecedor_b.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::cadastra_fornecedor) );

    v_box.append(fazer_pedido_b);
    fazer_pedido_b.set_expand();
    fazer_pedido_b.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::fazer_pedido) );

    v_box.append(consulta_preco_compra_b);
    consulta_preco_compra_b.set_expand();
    consulta_preco_compra_b.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::consulta_preco_compra) );

    v_box.append(mostra_todos_produtos_b);
    mostra_todos_produtos_b.set_expand();
    mostra_todos_produtos_b.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::mostra_todos_produtos) );


    v_box.append(mostra_fornecedores_b);
    mostra_fornecedores_b.set_expand();
    mostra_fornecedores_b.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::mostra_fornecedores) );

    v_box.append(mostra_produtos_caminho_b);
    mostra_produtos_caminho_b.set_expand();
    mostra_produtos_caminho_b.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::mostra_produtos_caminho) );

    v_box.append(mostra_produtos_pedido_b);
    mostra_produtos_pedido_b.set_expand();
    mostra_produtos_pedido_b.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::mostra_produtos_pedido) );

    quit_button.signal_clicked().connect( sigc::mem_fun(*this,
              &FinanceiroW::on_quit_clicked) );
    v_box.append(quit_button);
    quit_button.set_expand();

    reload_patrimonio();

}

void FinanceiroW::cadastra_fornecedor()
{
    Glib::RefPtr<Gtk::Application> app = get_application();
	//std::vector<Window*> windows = app->get_windows();
	
	app->add_window(cadastra_fornecedor_w);
	//mainWindow.Create(); //Create just calls show() on the window, tried it as a simple double check, didn't work
    
    cadastra_fornecedor_w.set_db(&db);

    cadastra_fornecedor_w.signal_close_request().connect([this]() -> bool {
        this->cadastra_fornecedor_w.hide();
        return true; 
    }, false);
    
    //financeiro = new FinanceiroW;
    //cadastra_fornecedor_w.signal_hide().connect(sigc::mem_fun(*this, &FinanceiroW::mostra_produtos_pedido_close));
    cadastra_fornecedor_w.show();
}

void FinanceiroW::cadastra_fornecedor_close()
{
    
}



void FinanceiroW::cadastra_produto()
{
    Glib::RefPtr<Gtk::Application> app = get_application();
	//std::vector<Window*> windows = app->get_windows();
	
	app->add_window(cadastra_produto_w);
	//mainWindow.Create(); //Create just calls show() on the window, tried it as a simple double check, didn't work
    
    cadastra_produto_w.set_db(&db);

    cadastra_produto_w.signal_close_request().connect([this]() -> bool {
        this->cadastra_produto_w.hide();
        return true; 
    }, false);
    
    //financeiro = new FinanceiroW;
    //cadastra_produto_w.signal_hide().connect(sigc::mem_fun(*this, &FinanceiroW::mostra_produtos_pedido_close));
    cadastra_produto_w.show();
}

void FinanceiroW::cadastra_produto_close()
{
    
}

void FinanceiroW::define_info_venda_produto()
{
    Glib::RefPtr<Gtk::Application> app = get_application();
	//std::vector<Window*> windows = app->get_windows();
	
	app->add_window(define_info_venda_produto_w);
	//mainWindow.Create(); //Create just calls show() on the window, tried it as a simple double check, didn't work
    
    define_info_venda_produto_w.set_db(&db);

    define_info_venda_produto_w.signal_close_request().connect([this]() -> bool {
        this->define_info_venda_produto_w.hide();
        return true; 
    }, false);
    
    //financeiro = new FinanceiroW;
    //define_info_venda_produto_w.signal_hide().connect(sigc::mem_fun(*this, &FinanceiroW::mostra_produtos_pedido_close));
    define_info_venda_produto_w.show();
}

void FinanceiroW::define_info_venda_produto_close()
{
    
}

void FinanceiroW::fazer_pedido()
{
    Glib::RefPtr<Gtk::Application> app = get_application();
	//std::vector<Window*> windows = app->get_windows();
	
	app->add_window(fazer_pedido_w);
	//mainWindow.Create(); //Create just calls show() on the window, tried it as a simple double check, didn't work
    
    fazer_pedido_w.set_db(&db);

    fazer_pedido_w.signal_close_request().connect([this]() -> bool {
        this->fazer_pedido_w.hide();
        return true; 
    }, false);
    
    //financeiro = new FinanceiroW;
    //fazer_pedido_w.signal_hide().connect(sigc::mem_fun(*this, &FinanceiroW::mostra_produtos_pedido_close));
    fazer_pedido_w.show();
}

void FinanceiroW::fazer_pedido_close()
{
    
}

void FinanceiroW::consulta_preco_compra()
{
    Glib::RefPtr<Gtk::Application> app = get_application();
	//std::vector<Window*> windows = app->get_windows();
	
	app->add_window(consulta_preco_compra_w);
	//mainWindow.Create(); //Create just calls show() on the window, tried it as a simple double check, didn't work
    
    consulta_preco_compra_w.set_db(&db);

    consulta_preco_compra_w.signal_close_request().connect([this]() -> bool {
        this->consulta_preco_compra_w.hide();
        return true; 
    }, false);
    
    //financeiro = new FinanceiroW;
    consulta_preco_compra_w.signal_hide().connect(sigc::mem_fun(*this, &FinanceiroW::mostra_produtos_pedido_close));
    consulta_preco_compra_w.show();
}

void FinanceiroW::consulta_preco_compra_close()
{
    
}

void FinanceiroW::mostra_todos_produtos()
{
    std::vector<std::vector<std::string>> tabela = db.get_tabela("produto");
    //db.mercado_dbAPI_connect();


    Glib::RefPtr<Gtk::Application> app = get_application();
	std::vector<Window*> windows = app->get_windows();
	
    app->add_window(mostra_todos_produtos_w);

    mostra_todos_produtos_w.set_table(tabela);

    mostra_todos_produtos_w.signal_close_request().connect([this]() -> bool {
        this->mostra_todos_produtos_w.hide();
        return true; 
    }, false);
    
    //mostra_todos_produtos_w.signal_hide().connect(sigc::mem_fun(*this, &Mostra_produtos_pedido::on_close));
    mostra_todos_produtos_w.show();
}

void FinanceiroW::mostra_todos_produtos_close()
{
    
}

void FinanceiroW::mostra_fornecedores()
{
    std::vector<std::vector<std::string>> tabela = db.get_tabela("fornecedor");
    //db.mercado_dbAPI_connect();


    Glib::RefPtr<Gtk::Application> app = get_application();
	std::vector<Window*> windows = app->get_windows();
	
    app->add_window(mostra_fornecedores_w);

    mostra_fornecedores_w.set_table(tabela);

    mostra_fornecedores_w.signal_close_request().connect([this]() -> bool {
        this->mostra_fornecedores_w.hide();
        return true; 
    }, false);
    
    //mostra_fornecedores_w.signal_hide().connect(sigc::mem_fun(*this, &Mostra_produtos_pedido::on_close));
    mostra_fornecedores_w.show();
}

void FinanceiroW::mostra_fornecedores_close()
{
    
}

void FinanceiroW::mostra_produtos_caminho()
{
    
    std::vector<std::vector<std::string>> tabela = db.get_produtos_caminho();
    //db.mercado_dbAPI_connect();


    Glib::RefPtr<Gtk::Application> app = get_application();
	std::vector<Window*> windows = app->get_windows();
	
    app->add_window(mostra_produtos_caminho_w);

    mostra_produtos_caminho_w.set_table(tabela);

    mostra_produtos_caminho_w.signal_close_request().connect([this]() -> bool {
        this->mostra_produtos_caminho_w.hide();
        return true; 
    }, false);
    
    //mostra_produtos_caminho_w.signal_hide().connect(sigc::mem_fun(*this, &Mostra_produtos_pedido::on_close));
    mostra_produtos_caminho_w.show();
}

void FinanceiroW::mostra_produtos_caminho_close()
{
    
}

void FinanceiroW::mostra_produtos_pedido()
{
    
    Glib::RefPtr<Gtk::Application> app = get_application();
	//std::vector<Window*> windows = app->get_windows();
	
	app->add_window(mostra_produtos_pedido_w);
	//mainWindow.Create(); //Create just calls show() on the window, tried it as a simple double check, didn't work
    
    mostra_produtos_pedido_w.set_db(&db);

    mostra_produtos_pedido_w.signal_close_request().connect([this]() -> bool {
        this->mostra_produtos_pedido_w.hide();
        return true; 
    }, false);
    
    //financeiro = new FinanceiroW;
    mostra_produtos_pedido_w.signal_hide().connect(sigc::mem_fun(*this, &FinanceiroW::mostra_produtos_pedido_close));
    mostra_produtos_pedido_w.show();
}

void FinanceiroW::mostra_produtos_pedido_close()
{
    
}

void FinanceiroW::on_quit_clicked(){
    set_visible(false);
}

void FinanceiroW::reload_patrimonio()
{
    std::string pat = db.get_saldo();
    
    l_patrimonio.set_text(pat);
}
