#include "../../include/financeiro/mostra_prod_ped.hpp"

Mostra_produtos_pedido::Mostra_produtos_pedido()
:
    confirm("Confirmar"),
    quit("Sair"),
    v_box(Gtk::Orientation::VERTICAL),
    label_pedido("Numero do pedido: ", Gtk::Align::START),
    adjustment_pedido( Gtk::Adjustment::create(1.0, 1.0, 1000.0, 1.0, 5.0, 0.0) ),
    spinButton_pedido(adjustment_pedido)
{
    
    set_default_size(200, 100);
    set_title("Mostra pedidos");

    set_child(v_box);

    

    label_pedido.set_expand();
    v_box.append(label_pedido);
    v_box.append(spinButton_pedido);
    spinButton_pedido.set_wrap();
    spinButton_pedido.set_expand();

    v_box.set_spacing(5);


    quit.signal_clicked().connect( sigc::mem_fun(*this,
              &Mostra_produtos_pedido::on_quit) );
    h_box_button.append(quit);
    quit.set_expand();

    confirm.signal_clicked().connect( sigc::mem_fun(*this,
              &Mostra_produtos_pedido::on_confirm) );
    h_box_button.set_spacing(5);
    h_box_button.append(confirm);
    confirm.set_expand();


    v_box.set_spacing(10);
    v_box.append(h_box_button);

}

void Mostra_produtos_pedido::on_confirm()
{   
   
    
    int pedido = spinButton_pedido.get_value_as_int();
    
    
    
    std::vector<std::vector<std::string>> tabela = db->get_produtos_pedido(pedido);
    //db.mercado_dbAPI_connect();


    Glib::RefPtr<Gtk::Application> app = get_application();
	  std::vector<Window*> windows = app->get_windows();
	
    app->add_window(lista);

    lista.set_table(tabela, std::to_string(pedido));

    lista.signal_close_request().connect([this]() -> bool {
        this->lista.hide();
        return true; 
    }, false);
    
    //lista.signal_hide().connect(sigc::mem_fun(*this, &Mostra_produtos_pedido::on_close));
    lista.show();
}

void Mostra_produtos_pedido::on_quit()
{
    set_visible(false);
}

void Mostra_produtos_pedido::on_close()
{
   
}

void Mostra_produtos_pedido::set_db(mercado_dbAPI* db_financeiro)
{
    db = db_financeiro;
    
}

Mostra_produtos_pedido_lista::Mostra_produtos_pedido_lista()
: 
    m_VBox(Gtk::Orientation::VERTICAL),
    m_Button_Quit("Sair")
{
  set_title(pedido);
  set_default_size(300, 250);

  m_VBox.set_margin(5);
  set_child(m_VBox);

  // Add the ColumnView, inside a ScrolledWindow, with the button underneath:
  m_ScrolledWindow.set_child(m_ColumnView);

  // Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
  m_ScrolledWindow.set_expand();

  m_VBox.append(m_ScrolledWindow);
  m_VBox.append(m_ButtonBox);

  m_ButtonBox.append(m_Button_Quit);
  m_ButtonBox.set_margin(5);
  m_Button_Quit.set_hexpand(true);
  m_Button_Quit.set_halign(Gtk::Align::END);
  m_Button_Quit.signal_clicked().connect(
    sigc::mem_fun(*this, &Mostra_produtos_pedido_lista::on_button_quit));

  // Create the List model:
  m_ListStore = Gio::ListStore<ModelColumns>::create();
  
  int row_max = tabela.size();
  for (int row=0; row<row_max; row++) {
        m_ListStore->append(ModelColumns::create(tabela[row][0], tabela[row][1], tabela[row][2]));
    }

  // Set list model and selection model.
  auto selection_model = Gtk::SingleSelection::create(m_ListStore);
  selection_model->set_autoselect(false);
  selection_model->set_can_unselect(true);
  m_ColumnView.set_model(selection_model);
  m_ColumnView.add_css_class("data-table"); // high density table

  // Make the columns reorderable.
  // This is not necessary, but it's nice to show the feature.
  m_ColumnView.set_reorderable(true);

  // Add the ColumnView's columns:

  // Id column
  auto factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Mostra_produtos_pedido_lista::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Mostra_produtos_pedido_lista::on_bind_produtos));
  auto column = Gtk::ColumnViewColumn::create("Produto", factory);
  m_ColumnView.append_column(column);

  // Name column
  factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Mostra_produtos_pedido_lista::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Mostra_produtos_pedido_lista::on_bind_quant));
  column = Gtk::ColumnViewColumn::create("Quantidade", factory);
  m_ColumnView.append_column(column);

  // Number column
  factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &Mostra_produtos_pedido_lista::on_setup_label), Gtk::Align::START));
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Mostra_produtos_pedido_lista::on_bind_valor));
  column = Gtk::ColumnViewColumn::create("Valor", factory);
  m_ColumnView.append_column(column);

}

void Mostra_produtos_pedido_lista::set_table(std::vector<std::vector<std::string>> tabela_f, std::string pedido_f){

    tabela = tabela_f;
    pedido = pedido_f;
    m_ListStore = Gio::ListStore<ModelColumns>::create();
  
    int row_max = tabela.size();
    for (int row=0; row<row_max; row++) {
          m_ListStore->append(ModelColumns::create(tabela[row][0], tabela[row][1], tabela[row][2]));
      }

    // Set list model and selection model.
    auto selection_model = Gtk::SingleSelection::create(m_ListStore);
    selection_model->set_autoselect(false);
    selection_model->set_can_unselect(true);
    m_ColumnView.set_model(selection_model);
    m_ColumnView.add_css_class("data-table"); // high density table

    // Make the columns reorderable.
    // This is not necessary, but it's nice to show the feature.
    m_ColumnView.set_reorderable(true);
    set_title(pedido);
}

void Mostra_produtos_pedido_lista::on_button_quit()
{
  set_visible(false);
}

void Mostra_produtos_pedido_lista::on_setup_label(
  const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign)
{
  list_item->set_child(*Gtk::make_managed<Gtk::Label>("", halign));
}


void Mostra_produtos_pedido_lista::on_bind_produtos(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_produtos);
}

void Mostra_produtos_pedido_lista::on_bind_quant(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_quant);
}

void Mostra_produtos_pedido_lista::on_bind_valor(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto col = std::dynamic_pointer_cast<ModelColumns>(list_item->get_item());
  if (!col)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(col->m_col_valor);
}