#ifndef DPAPI_H_INCLUDED
#define DBAPI_H_INCLUDED
#include <postgresql/libpq-fe.h>  // Biblioteca para acessar o banco de dados
#include <iostream>
#include <string>
#include <vector>
#include <optional>

class mercado_dbAPI{
  private:
    PGconn *conn;  // Conexao com a db
    int execCommand(std::string command);
    std::optional<std::string> callFunction(std::string function);
    int get_ultimo_pedido_id();
    std::vector<std::vector<std::string>> tabela_from_query(std::string query);
  public:
    mercado_dbAPI();
    mercado_dbAPI(std::string conn_type);
    ~mercado_dbAPI();
    int testConn();

    // FINANCEIRO
    int adiciona_saldo(std::string tipo, int valor);
    int registra_fornecedor(std::string nome, std::string cnpj);
    int registra_produto(std::string nome);
    int atualiza_preco_produto(int produto_id, float novo_preco);
    int atualiza_quant_min_produto(int produto_id, int quant_min_gondola, int quant_min_estoque);
    int abre_pedido(int fornecedor_id);
    int adiciona_pedido_produto(int pedido_id, int produto_id, int quantidade, float valor);
    int fecha_pedido(int pedido_id, std::string data_prevista);

    std::vector<std::string> get_valores_compra(int produto_id);
    std::string get_nome_produto(int produto_id);
    std::string get_saldo();

    // ESTOQUE
    int add_produto_estoque(int produto_id, int quantidade);
    int move_produto_estoque_gondola(int produto_id, int quantidade);
    std::optional<int> get_quant_estoque_from_barcode(std::string barcode);

    //GONDOLA
    int atualiza_quant_gondola(int produto_id, int quantidade);

    // CONSULTA
    std::vector<std::vector<std::string>> get_tabela(std::string tabela);
    std::vector<std::vector<std::string>> get_produtos_caminho();
    std::vector<std::vector<std::string>> get_produtos_pedido(int pedido_id);
    std::vector<std::vector<std::string>> get_produtos_com_preco();
};

#endif
