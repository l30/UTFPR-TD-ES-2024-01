#ifndef FINANCEIROW_H_INCLUDED
#define FINANCEIROW_H_INCLUDED

#include "./mostra_prod_ped.hpp"
#include "./mostra_ped_cam.hpp"
#include "./mostra_fornecedores.hpp"
#include "./mostra_produtos.hpp"
#include "./consulta_preco_compra.hpp"
#include "./fazer_pedido.hpp"
#include "./cadastra_fornecedor.hpp"
#include "./define_preco_venda.hpp"
#include "./cadastra_produto.hpp"
#include "./financeiroWidgets.hpp"

#include <gtkmm.h>


class FinanceiroW: public Gtk::Window
{

public:
  FinanceiroW(); 
protected:

  mercado_dbAPI db;

  Cadastra_fornecedor cadastra_fornecedor_w;
  Cadastra_produto cadastra_produto_w;
  Define_preco_venda define_info_venda_produto_w;
  Fazer_pedido fazer_pedido_w;
  Consulta_preco_compra consulta_preco_compra_w;
  Mostra_produtos mostra_todos_produtos_w;
  Mostra_forncedores mostra_fornecedores_w;
  Mostra_produtos_caminho mostra_produtos_caminho_w;
  Mostra_produtos_pedido mostra_produtos_pedido_w;

  void cadastra_fornecedor();
  void cadastra_produto();
  void define_info_venda_produto();
  void fazer_pedido();
  void consulta_preco_compra();
  void mostra_todos_produtos();
  void mostra_fornecedores();
  void mostra_produtos_caminho();
  void mostra_produtos_pedido();

  void cadastra_fornecedor_close();
  void cadastra_produto_close();
  void define_info_venda_produto_close();
  void fazer_pedido_close();
  void consulta_preco_compra_close();
  void mostra_todos_produtos_close();
  void mostra_fornecedores_close();
  void mostra_produtos_caminho_close();
  void mostra_produtos_pedido_close();

  void on_quit_clicked();

  void reload_patrimonio();

  Gtk::Box v_box;

  Gtk::Frame f_patrimonio;
  Gtk::Box h_patrimonio;
  Gtk::Label l_patrimonio;
  Gtk::Button b_patrimonio;

  Gtk::Button cadastra_fornecedor_b;
  Gtk::Button cadastra_produto_b;
  Gtk::Button define_info_venda_produto_b;
  Gtk::Button fazer_pedido_b;
  Gtk::Button consulta_preco_compra_b;
  Gtk::Button mostra_todos_produtos_b;
  Gtk::Button mostra_fornecedores_b;
  Gtk::Button mostra_produtos_caminho_b;
  Gtk::Button mostra_produtos_pedido_b;

  Gtk::Button quit_button;

};

#endif