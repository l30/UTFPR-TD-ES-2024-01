#ifndef MOSTRA_CONSULTA_PRECO_COMPRA_H_INCLUDED
#define MOSTRA_CONSULTA_PRECO_COMPRA_H_INCLUDED
#include <gtkmm.h>
#include <string>
#include <vector>
#include "./financeiroWidgets.hpp"

class Lista_valores: public Gtk::Window {
public:
    Lista_valores();
    void set_table(std::vector<std::string> tabela_f, std::string pedido_f);
protected:

  
    // Signal handlers:
  void on_button_quit();
  void on_setup_label(const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign);
  void on_bind_valor(const Glib::RefPtr<Gtk::ListItem>& list_item);
  

    
  // A Gio::ListStore item.
  class ModelColumns : public Glib::Object
  {
  public:
    Glib::ustring m_col_valor;

    static Glib::RefPtr<ModelColumns> create(const Glib::ustring& valor)
    {
      return Glib::make_refptr_for_instance<ModelColumns>(
        new ModelColumns(valor));
    }

  protected:
    ModelColumns(const Glib::ustring& valor)
    : m_col_valor(valor)
    {}
  }; // ModelColumns

  std::vector<std::string> tabela;
  std::string pedido;

  // Child widgets:
  Gtk::Box m_VBox;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::ColumnView m_ColumnView;
  Gtk::Box m_ButtonBox;
  Gtk::Button m_Button_Quit;

  Glib::RefPtr<Gio::ListStore<ModelColumns>> m_ListStore;

};

class Consulta_preco_compra: public Gtk::Window {
    public:
        Consulta_preco_compra();
        void set_db(mercado_dbAPI* db_financeiro);
    protected:

        mercado_dbAPI* db;

        void on_confirm();
        void on_quit();
        void on_close();


        Lista_valores lista;
        Gtk::Button confirm;
        Gtk::Button quit;
        Glib::RefPtr<Gtk::Adjustment> adjustment_pedido;
        Gtk::SpinButton spinButton_pedido;
        Gtk::Box v_box, h_box_button;
        Gtk::Label label_pedido, label_produto, label_valor;

    };

#endif