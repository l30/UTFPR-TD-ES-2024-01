#ifndef MOSTRA_FORNCEDORES_H_INCLUDED
#define MOSTRA_FORNCEDORES_H_INCLUDED
#include <gtkmm.h>
#include <string>
#include <vector>
#include "./financeiroWidgets.hpp"

class Mostra_forncedores: public Gtk::Window {
public:
    Mostra_forncedores();
    void set_table(std::vector<std::vector<std::string>> tabela_f);
protected:

  
    // Signal handlers:
  void on_button_quit();
  void on_setup_label(const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign);
  void on_bind_forn_id(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_nome(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_cnpj(const Glib::RefPtr<Gtk::ListItem>& list_item);
  

    
  // A Gio::ListStore item.
  class ModelColumns : public Glib::Object
  {
  public:
    Glib::ustring m_col_forn_id;
    Glib::ustring m_col_nome;
    Glib::ustring m_col_cnpj;

    static Glib::RefPtr<ModelColumns> create(const Glib::ustring& fornecedor_id, const Glib::ustring& nome, const Glib::ustring& cnpj)
    {
      return Glib::make_refptr_for_instance<ModelColumns>(
        new ModelColumns(fornecedor_id, nome, cnpj));
    }

  protected:
    ModelColumns(const Glib::ustring& fornecedor_id, const Glib::ustring& nome, const Glib::ustring& cnpj)
    : m_col_forn_id(fornecedor_id), m_col_nome(nome), m_col_cnpj(cnpj)
    {}
  }; // ModelColumns

  std::vector<std::vector<std::string>> tabela;

  // Child widgets:
  Gtk::Box m_VBox;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::ColumnView m_ColumnView;
  Gtk::Box m_ButtonBox;
  Gtk::Button m_Button_Quit;

  Glib::RefPtr<Gio::ListStore<ModelColumns>> m_ListStore;

};


#endif