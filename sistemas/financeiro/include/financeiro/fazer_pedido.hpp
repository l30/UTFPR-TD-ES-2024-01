#ifndef MOSTRA_FAZER_PEDIDO_H_INCLUDED
#define MOSTRA_FAZER_PEDIDO_H_INCLUDED
#include <gtkmm.h>
#include <string>
#include <vector>
#include "./financeiroWidgets.hpp"

class Fechar_pedido: public Gtk::Window {
    public:
        Fechar_pedido();
        void set_db(mercado_dbAPI* db_financeiro, int pedido);
    protected:

        mercado_dbAPI* db;
        int pedido_id;

        void on_confirm();
        
        Gtk::Button confirm;
        Gtk::Box v_box, h_box_button, h_box, v_box_day, v_box_month, v_box_year;
        Gtk::Label label_day, label_month, label_year;
        Glib::RefPtr<Gtk::Adjustment> m_adjustment_day, m_adjustment_month, m_adjustment_year;
        Gtk::SpinButton m_SpinButton_day, m_SpinButton_month, m_SpinButton_year;

    };

class Adicionar: public Gtk::Window {
    public:
        Adicionar();
        void set_db(mercado_dbAPI* db_financeiro, int pedido);
        int get_quant();
        int get_id();
        double get_valor();
        int get_res();
    protected:

        mercado_dbAPI* db;

        void on_confirm();
        void on_quit();
        
        int pedido_id;
        int id;
        int quant;
        double valor;
        int res;
        Gtk::Button confirm;
        Gtk::Button quit;
        Gtk::Box v_box, h_box_button, h_box, v_box_id, v_box_quant, v_box_valor;
        Gtk::Label label_id, label_quant, label_valor;
        Glib::RefPtr<Gtk::Adjustment> m_adjustment_id, m_adjustment_quant, m_adjustment_valor;
        Gtk::SpinButton m_SpinButton_id, m_SpinButton_quant, m_SpinButton_valor;

    };


class Lista_pedidos: public Gtk::Window {
public:
    Lista_pedidos();
    void set_db(mercado_dbAPI* db_financeiro, int pedido_f);
protected:

    mercado_dbAPI* db;
  
    // Signal handlers:
  void on_button_quit();
  void on_button_adicionar();
  void on_close_adicionar();
  void on_close_fechar();
  void on_setup_label(const Glib::RefPtr<Gtk::ListItem>& list_item, Gtk::Align halign);
  void on_bind_produtos(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_quant(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_valor(const Glib::RefPtr<Gtk::ListItem>& list_item);
  

    
  // A Gio::ListStore item.
  class ModelColumns : public Glib::Object
  {
  public:
    Glib::ustring m_col_produtos;
    Glib::ustring m_col_quant;
    Glib::ustring m_col_valor;

    static Glib::RefPtr<ModelColumns> create(const Glib::ustring& produtos,
      const Glib::ustring& quantidade, const Glib::ustring& valor)
    {
      return Glib::make_refptr_for_instance<ModelColumns>(
        new ModelColumns(produtos, quantidade, valor));
    }

  protected:
    ModelColumns(const Glib::ustring& produtos,
      const Glib::ustring& quantidade, const Glib::ustring& valor)
    : m_col_produtos(produtos), m_col_quant(quantidade), m_col_valor(valor)
    {}
  }; // ModelColumns

  int pedido_status;
  int pedido;

  // Child widgets:
  Fechar_pedido fecha_pedido;
  Adicionar adiciona;
  Gtk::Box m_VBox;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::ColumnView m_ColumnView;
  Gtk::Box m_ButtonBox;
  Gtk::Label label_erro;
  Gtk::Button m_Button_Adicionar;
  Gtk::Button m_Button_Quit;

  Glib::RefPtr<Gio::ListStore<ModelColumns>> m_ListStore;

};


class Fazer_pedido: public Gtk::Window {
    public:
        Fazer_pedido();
        void set_db(mercado_dbAPI* db_financeiro);
    protected:

        mercado_dbAPI* db;
        int pedido_status;

        void on_confirm();
        void on_quit();
        void on_close();
        

        Lista_pedidos lista;
        Gtk::Button confirm;
        Gtk::Button quit;
        Glib::RefPtr<Gtk::Adjustment> adjustment_pedido;
        Gtk::SpinButton spinButton_pedido;
        Gtk::Box v_box, h_box_button;
        Gtk::Label label_pedido;
        Gtk::Label label_erro;

    };

#endif